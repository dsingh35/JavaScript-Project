window.onload = () =>{
    if(window.Worker){
        var myWorker = new Worker("worker.js");
        var canvas = document.getElementById("myCanvas");
        var ctx = canvas.getContext("2d");
        var img = new Image(500,500);
        img.src = "img.jpg";
        var imgData;

        img.onload = function() {
            ctx.drawImage(img, 0, 0);
            imgData = ctx.getImageData(0,0,500,500);
        
        console.log(imgData);
        myWorker.postMessage(imgData);
        
        setInterval( () => {
            myWorker.onmessage = (result) => {
            imgData = result.data;
                    ctx.clearRect(0,0,500,500);
                    ctx.putImageData(imgData,0,0);
            }
        myWorker.postMessage(imgData);
       },500);
    };
    }
}
